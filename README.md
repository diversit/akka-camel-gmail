# Reading [g]mail using Akka and Apache Camel

## Intro
Small sample project to show how to read [g]mail messages using Akka and Apache Camel.

The Akka-Camel extention provides a nice integration of Camel into Akka.
This makes it easy to use the Camel-Mail component to get message from any IMAP or POP3 provider
and process those messages asynchronously using an Actor.

## The problem
Apparently, the JavaMail API lazily reads the body of the message.
Therefore it is not possible to read the content or body of a mail message in an Actor.
As soon as you try to read the content from a BodyPart instance, a FolderClosedException is thrown.
This is logical. Since the message is process asynchronously in the actor, the Camel Mail component has already closed
the connection to the mail Store (the Mail component opens and closes the connection for each poll).

## The solution
The solution is that you have to preprocess the MailMessage and get the message body before dispatching it to the actor.
One way of doing that is shown in this project.
It's done by creating a route in code from the Mail endpoint through a processor to the target actor.
The processor gets the headers and message bodies from the MailMessage and puts them in an EmailMessage.
The actor will now receive a CamelMessage containing the EmailMessage which contains the bodies.

## Using the project
Checkout the project from [https://bitbucket.org/diversit/akka-camel-gmail]

>git clone https://bitbucket.org/diversit/akka-camel-gmail

Create a mail.properties in 'src/main/resources'.
Have a look at the mail.properties.example and [Camel Mail component](http://camel.apache.org/mail.html) how to create a mail endpoint uri.

Run the project using SBT

>sbt run

## More info
See [http://akka.io](http://akka.io) for more info about Akka and the Akka-Camel extention. It is very well documented in the Akka documentation (pdf).
See [http://camel.apache.org/mail.html](http://camel.apache.org/mail.html) for more info about the Camel Mail component.
See [http://www.scala-lang.org/](http://www.scala-lang.org/) for more info about Scala.