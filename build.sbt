name := "Akka Gmail with Camel"

organization := "eu.diversit"

version := "0.1.1"

scalaVersion := "2.10.0"

libraryDependencies ++= {
    val akkaVersion = "2.1.0"
    Seq(
      "com.typesafe.akka" %% "akka-actor" % akkaVersion,
      "com.typesafe.akka" %% "akka-camel" % akkaVersion,
      "org.apache.camel"   % "camel-mail" % "2.10.3",
      "org.slf4j"          % "slf4j-log4j12" % "1.7.2",
      "org.scalatest"      % "scalatest_2.10" % "2.0.M5b" % "test"
    )
}

