package eu.diversit.akka.email

import akka.actor.{Props, Actor, ActorSystem}
import org.slf4j.LoggerFactory
import akka.camel.{CamelExtension, CamelMessage}
import java.util.Properties

object Boot extends App {
  def getMailUri = {
    val props: Properties = new Properties()
    props.load(this.getClass.getClassLoader.getResourceAsStream("mail.properties"))
    props.getProperty("mail.uri")
  }

  val gmail = getMailUri

  val system = ActorSystem("gmail-client")
  val camel = CamelExtension(system)
  val gmailActor  = system.actorOf(Props[EmailConsumer])
  camel.context.addRoutes(EmailRoute(gmail, gmailActor))

  val logActor = system.actorOf(Props[LogActor])
  logActor ! "App started"
}

class EmailConsumer extends Actor {
  val logActor = context.actorOf(Props[LogActor])
  def receive = {
    case msg: CamelMessage => {
      // not possible to use msg.bodyAs[] because this actor no long extends Consumer
      // and thus has no implicit Camel context.
      val mail: EmailMessage = msg.body.asInstanceOf[EmailMessage]

      logActor ! ("Subject:"+mail.subject)
      logActor ! ("From:"+mail.subject)
      logActor ! ("Cc: "+mail.cc)
      logActor ! ("Body:"+mail.bodyAsText)
      //      logActor ! ("Html:"+mail.bodyAsHTML)
      logActor ! ("Headers:"+mail.headers)
    }
  }
}

class LogActor extends Actor {
  val logger = LoggerFactory.getLogger(this.getClass)

  def receive = {
    case msg => logger.info("Received: "+msg)
  }
}
