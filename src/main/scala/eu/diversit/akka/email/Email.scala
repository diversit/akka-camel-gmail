package eu.diversit.akka.email

import org.apache.camel.{Message, Exchange, Processor}
import org.apache.camel.component.mail.MailMessage
import javax.mail.internet.MimeMultipart
import akka.actor.{ActorRef, ActorSystem}
import org.apache.camel.builder.RouteBuilder

/**
 * Data holder for email content.
 * Contains the real headers and bodies or email message.
 *
 * @param _headers java.util.Map<String,Object>
 * @param bodies
 */
class EmailMessage(_headers:java.util.Map[String, AnyRef], bodies:Map[String, Any]) {
  import scala.collection.JavaConverters._ // to convert java type to scala type
  val headers = _headers.asScala.toMap

  val NO_VALUE = "No Value"
  private def headerValue(key: String) = headers.getOrElse(key, NO_VALUE).asInstanceOf[String]

  def subject      = headerValue("Subject")
  def to           = headerValue("To")
  def from         = headerValue("From")
  def cc           = headers.get("Cc").asInstanceOf[Option[String]]
  def date         = headerValue("Date")
  def bodyAsText   = bodies.getOrElse(ContentType.TEXT, NO_VALUE)
  def bodyAsHTML   = bodies.getOrElse(ContentType.HTML, "<html><body>%s</body></html>" format NO_VALUE)
}

private[email] object ContentType {
  val TEXT = "PLAIN"
  val HTML  = "HTML"

  private val regex = """[A-Z]*/([A-Z]*);.*""".r

  // Get content type from a String like "TEXT/PLAIN; more text; ..."
  def getFrom(str: String) = regex.findFirstMatchIn(str) match {
    case Some(matcher) => matcher.group(1)
    case _ => str
  }
}

/**
 * Processor to get bodies of a {@link org.apache.camel.component.mail.MailMessageage} with a MimeMultiPart body
 * and put those in a new {@link eu.diversit.akka.gmail.EmailMessage} instance.
 */
private[email] class EmailMessageProcessor extends Processor {

  def process(exchange: Exchange) = {
    // get all bodies from MailMessage
    def getBodies(msg:MailMessage) = {
      val body = msg.getBody.asInstanceOf[MimeMultipart]
      val bodyCount = body.getCount
      for {
        i <- 0 until bodyCount
      } yield (ContentType.getFrom(body.getBodyPart(i).getContentType), body.getBodyPart(i).getContent)
    }

    val in = exchange.getIn
    val bodies = getBodies(in.asInstanceOf[MailMessage]).toMap

    val newMsg: EmailMessage = new EmailMessage(in.getHeaders, bodies)
    val out: Message = exchange.getOut // creates a new out Message
    out.setHeaders(in.getHeaders) // reuse original headers
    out.setBody(newMsg) // set new message
  }
}

/**
 * Builder which creates a route from a mail source uri through a mail processor to a target actor.
 * The received {@link org.apache.camel.component.mail.MailMessage} needs to be processed first because
 * the actor is otherwise not able to retrieve the mail body content anymore, because the actor processes
 * the message asynchronously and then the connection to the mail Store is already close.
 *
 * @param mailUri a Mail source uri. Must start with 'imaps://', 'imap://', 'pop3s://' or 'pop3://'. See Camel Mail component.
 * @param target target actor
 */
class EmailRouteBuilder(mailUri: String, target: ActorRef) extends RouteBuilder {
  require(mailUri.startsWith("imaps://") || mailUri.startsWith("imap://") || mailUri.startsWith("pop3s://") || mailUri.startsWith("pop3://"))
  val mailProcessor = new EmailMessageProcessor

  import akka.camel._ // to import implicit toActorRouteDefinition method
  def configure() {
    from(mailUri).process(mailProcessor).to(target)
  }
}

object EmailRoute {
  def apply(mailUri: String, target: ActorRef) = new EmailRouteBuilder(mailUri, target)
}
